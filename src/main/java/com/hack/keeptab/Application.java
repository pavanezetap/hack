package com.hack.keeptab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@EnableAutoConfiguration
@ComponentScan(basePackages="com.hack.keeptab")
@EntityScan(value="com.hack.keeptab.entity")
public class Application {

	public static void main(String args[]) {
		SpringApplication.run(Application.class, args);
	}
}

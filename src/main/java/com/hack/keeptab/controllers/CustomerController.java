package com.hack.keeptab.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.hack.keeptab.entity.Customer;
import com.hack.keeptab.service.CustomerService;

@RestController
public class CustomerController{

	@Autowired CustomerService customerService;
	
	@RequestMapping(value="/")
	public String sayHi(){
		return "Welcome to KeepTab!";
	}
	
	@RequestMapping(value="/customers", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody public List<Customer> list(){
		return customerService.findAll();
	}
	
	@RequestMapping(value="/customers/{mobileNumber}", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody public Customer getDetails(@PathVariable String mobileNumber) {
		return customerService.findById(mobileNumber);
	}
	
	@RequestMapping(value="/customers", method = RequestMethod.PUT, consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody public Customer createOrUpdate(@RequestBody Customer c) {
		return customerService.createOrUpdate(c);
	}
	
	@RequestMapping(value="/customers/{mobileNumber}", method = RequestMethod.DELETE)
	@ResponseBody public void delete(@PathVariable String mobileNumber) {
		customerService.delete(mobileNumber);
	}
}

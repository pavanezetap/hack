package com.hack.keeptab.controllers;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.hack.keeptab.entity.Customer;
import com.hack.keeptab.entity.Txn;
import com.hack.keeptab.entity.bean.ApiResponse;
import com.hack.keeptab.entity.enums.TxnType;
import com.hack.keeptab.service.CustomerService;
import com.hack.keeptab.service.TxnService;

@RestController
@Transactional
public class TxnController{

	@Autowired CustomerService customerService;
	@Autowired TxnService txnService;
	
	@RequestMapping(value="/txn",method = RequestMethod.PUT, consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody public ApiResponse register(@RequestBody Txn txn) {
		Customer c = customerService.findById(txn.getMobileNumber());
		Txn t = txnService.findById(txn.getOrderId());
		if(c==null || t!=null || (txn.getAmount() > c.getCreditAmt() && txn.getTxnType().equals(TxnType.DEBIT))) {
			return new ApiResponse(false);
		}
		txn.setCreatedTime(new Date());
		Txn created = txnService.create(txn);
		if(txn.getTxnType().equals(TxnType.CREDIT)) {
			c.setCreditAmt(c.getCreditAmt() + txn.getAmount());
		}
		if(txn.getTxnType().equals(TxnType.DEBIT)) {
			c.setCreditAmt(c.getCreditAmt() - txn.getAmount());
		}
		Customer updated = customerService.createOrUpdate(c);
		
		System.out.println("Txn created "+created.toString());
		System.out.println("Customer updated "+updated.toString());
		
		return new ApiResponse(true);
	}
	
	@RequestMapping(value="/txns/{mobileNumber}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody public List<Txn> list(@PathVariable String mobileNumber){
		return txnService.getAllForCustomer(mobileNumber);
	}
}

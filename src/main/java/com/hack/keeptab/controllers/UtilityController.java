package com.hack.keeptab.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hack.keeptab.entity.Customer;
import com.hack.keeptab.service.CustomerService;
import com.hack.keeptab.service.TxnService;

@RestController
public class UtilityController {
	@Autowired CustomerService customerService;
	@Autowired TxnService txnService;

	@RequestMapping(value="/cleandb", method=RequestMethod.GET)
	public void cleanDb() {
		customerService.deleteAll();
		txnService.deleteAll();
	}
	
	@RequestMapping(value="/seeddata", method=RequestMethod.GET)
	public void seeddata() {
		Customer c1 = new Customer();
		c1.setAddress("HSR");
		c1.setCreditAmt(0d);
		c1.setMobileNumber("7353552244");
		c1.setName("Pavan");		
		customerService.createOrUpdate(c1);
		
		Customer c2 = new Customer();
		c2.setAddress("BTM");
		c2.setCreditAmt(0d);
		c2.setMobileNumber("9876543211");
		c2.setName("Khaja");
		customerService.createOrUpdate(c2);
	}
}

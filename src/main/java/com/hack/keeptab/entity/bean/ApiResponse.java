package com.hack.keeptab.entity.bean;

public class ApiResponse {

	private boolean results;
	
	public ApiResponse(boolean results) {;
		this.results = results;
	}

	public boolean isResults() {
		return results;
	}

	public void setResults(boolean results) {
		this.results = results;
	}
}

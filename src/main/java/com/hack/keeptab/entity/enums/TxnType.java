package com.hack.keeptab.entity.enums;

public enum TxnType {
	CREDIT, DEBIT
}

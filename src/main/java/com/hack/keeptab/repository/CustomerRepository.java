package com.hack.keeptab.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hack.keeptab.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, String> {

}

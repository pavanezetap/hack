package com.hack.keeptab.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hack.keeptab.entity.Txn;

public interface TxnRepository extends JpaRepository<Txn, String>{
	
    public List<Txn> findByMobileNumber(String mobileNumber);
}

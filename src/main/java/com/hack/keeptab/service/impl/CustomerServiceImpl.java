package com.hack.keeptab.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.hack.keeptab.entity.Customer;
import com.hack.keeptab.repository.CustomerRepository;
import com.hack.keeptab.service.CustomerService;


@Service("customerService")
public class CustomerServiceImpl implements CustomerService{

	@Resource CustomerRepository customerRepository;
	
	@Override
	public Customer createOrUpdate(Customer c) {
		return customerRepository.save(c);
	}
	
	@Override
	public List<Customer> findAll(){
		return customerRepository.findAll();
	}

	@Override
	public Customer findById(String mobile) {
		return customerRepository.findOne(mobile);
	}

	@Override
	public void delete(String mobile) {
		customerRepository.delete(mobile);
	}
	
	@Override
	public void deleteAll() {
		customerRepository.deleteAll();
	}
}

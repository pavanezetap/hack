package com.hack.keeptab.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.hack.keeptab.entity.Txn;
import com.hack.keeptab.repository.TxnRepository;
import com.hack.keeptab.service.TxnService;

@Service("txnService")
public class TxnServiceImpl implements TxnService {
	
	@Resource TxnRepository txnRepository;
	
	@Override
	public Txn create(Txn txn){
		return txnRepository.save(txn);
	}

	@Override
	public Txn findById(String orderId) {
		return txnRepository.findOne(orderId);
	}

	@Override
	public List<Txn> getAllForCustomer(String mobile) {
		return txnRepository.findByMobileNumber(mobile);		
	}

	@Override
	public void deleteAll() {
		txnRepository.deleteAll();
	}	
	
}

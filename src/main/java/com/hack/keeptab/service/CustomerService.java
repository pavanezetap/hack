package com.hack.keeptab.service;

import java.util.List;

import com.hack.keeptab.entity.Customer;


public interface CustomerService {

	public Customer createOrUpdate(Customer c);
	public List<Customer> findAll();
	public Customer findById(String mobile);
	public void delete(String mobile);
	public void deleteAll();
}

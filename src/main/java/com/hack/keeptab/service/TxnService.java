package com.hack.keeptab.service;

import java.util.List;

import com.hack.keeptab.entity.Txn;

public interface TxnService {

	public Txn create(Txn txn);
	public Txn findById(String orderId);
	public List<Txn> getAllForCustomer(String mobileNumber);
	public void deleteAll();
}
